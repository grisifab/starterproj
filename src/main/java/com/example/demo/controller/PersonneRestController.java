package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PersonneRepository;
import com.example.demo.models.Personne;

@RestController
public class PersonneRestController {
	@Autowired
	private PersonneRepository personneRepository;
	
	@GetMapping("/personnes")
	@ResponseBody
	public String getPersonnes() {
		return personneRepository.findAll().toString();
	}

	@GetMapping("/personnes/{id}")
	@ResponseBody
	public String getPersonne(@PathVariable("id") long id) {
		return personneRepository.findById(id).orElse(null).toString();
	}

	@RequestMapping(value = "/chercherPersonnes", method = RequestMethod.GET)
	public Page<Personne> chercher(
			@RequestParam(name = "mc", defaultValue = "") String mc,
			@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "5") int size) {
		Page<Personne> p = personneRepository.chercher(mc, new PageRequest(page, size));
		return p;
	}
	
//	@PostMapping("/personnes")
//	@ResponseBody
//	public String addPersonne(Personne personne) {
//		System.out.println(personne);
//		return personneRepository.save(personne).toString();
//	}
	
	@PostMapping("/personnes")
	@ResponseBody
	public Personne addPersonne(@RequestBody Personne personne) {
		System.out.println(personne);
		return personneRepository.save(personne);
	}
	
	
	@PutMapping("/personnes/{id}")
	public Personne updatePersonne(@PathVariable("id") long id, @RequestBody Personne personne) {
		personne.setNum(id);
		return personneRepository.save(personne);
	}	
	
	@DeleteMapping("/personnes/{id}")
	public boolean deletePersonne(@PathVariable("id") long id) {
		personneRepository.deleteById(id);
		return true;
	}
	
}